﻿-- 云市场导航栏


-- 开源版
 INSERT INTO `ocenter_m_menu` VALUES
  (92, '备份数据库', 113, 8, 'Database/index?type=export', 0, '', '数据备份', 0, '', ''),
  (93, '备份', 92, 0, 'Database/export', 0, '备份数据库', '', 0, '', ''),
  (94, '优化表', 92, 0, 'Database/optimize', 0, '优化数据表', '', 0, '', ''),
  (95, '修复表', 92, 0, 'Database/repair', 0, '修复数据表', '', 0, '', ''),
  (96, '还原数据库', 113, 9, 'Database/index?type=import', 0, '', '数据备份', 0, '', ''),
  (97, '恢复', 96, 0, 'Database/import', 0, '数据库恢复', '', 0, '', ''),
  (98, '删除', 96, 0, 'Database/del', 0, '删除备份文件', '', 0, '', ''),
  (178,'云市场',105,1,'Cloud/index',0,'','云市场',0,'',''),
  (202,'自动升级',105,2,'Cloud/update',0,'','云市场',0,'','');

INSERT INTO `ocenter_m_auth_rule` (`id`, `module`, `type`, `name`, `title`, `status`, `condition`) VALUES
  (80, 'admin', 1, 'Admin/Database/index?type=export', '备份数据库', 1, ''),
  (81, 'admin', 1, 'Admin/Database/index?type=import', '还原数据库', 1, '');
